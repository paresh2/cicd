#!/bin/bash
set -euo pipefail
FAILURE=1
SUCCESS=0

SLACKWEBHOOKURL="https://hooks.slack.com/services/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"
function print_slack_summary_build() {
local slack_msg_header
    local slack_msg_body
    local slack_channel
# Populate header and define slack channels
slack_msg_header=":x: *Build to ${ENVIRONMENTNAME} failed*"
if [[ "${EXIT_STATUS}" == "${SUCCESS}" ]]; then
        slack_msg_header=":heavy_check_mark: *Build to ${ENVIRONMENTNAME} succeeded*"
        #slack_channel="$CHANNEL_TEST"
    fi
cat <<-SLACK
            {
                "blocks": [
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": "${slack_msg_header}"
                        }
                    },
                    {
                        "type": "divider"
                    },
                    {
                        "type": "section",
                        "fields": [
                            {
                                "type": "mrkdwn",
                                "text": "*Stage:*\nBuild"
                            },
                            {
                                "type": "mrkdwn",
                                "text": "*Pushed By:*\n${GITLAB_USER_LOGIN}"
                            },
                            {
                                "type": "mrkdwn",
                                "text": "*Job URL:*\ngitlab.com/${GITLAB_USER_LOGIN}/${CI_PROJECT_NAME}/-/jobs/${CI_JOB_ID}"
                            },
                            {
                                "type": "mrkdwn",
                                "text": "*Commit URL:*\ngitlab.com/${GITLAB_USER_LOGIN}/${CI_PROJECT_NAME}/-/tree/$(git rev-parse HEAD)"
                            },
                            {
                                "type": "mrkdwn",
                                "text": "*Commit Branch:*\n${CI_COMMIT_REF_NAME}"
                            }
                        ]
                    },
                    {
                        "type": "divider"
                    }
                ]
}
SLACK
}
function share_slack_update_build() {
local slack_webhook
slack_webhook="$SLACKWEBHOOKURL"
curl -X POST                                           \
        --data-urlencode "payload=$(print_slack_summary_build)"  \
        "${slack_webhook}"
}
